## Notes on Fiber Products

Let $A$, $B$, $D$ be algebras of the same type.  Let $f: A\to D$ and $g: B \to D$ be epimorphisms and consider the set 

$$C = \{(a, b)\in A \times B \mid f(a) = g(b)\}.$$

Since $C$ is a binary relation, it induces a Galois connection.

For $X \subseteq A$ and $Y \subseteq B$, let 

$$X^{\wedge} = \{b \in B \mid (x, b) \in C \; \forall x\in X\} = \{b \in B \mid  \forall x\in X \,.\, f(x) = g(b)\}$$ 
and

$$Y^\vee = \{a \in A \mid (a, y) \in C \; \forall y\in Y\}= \{a \in A \mid \forall y\in Y\,.\,f(a) = g(y)\}$$

Notice that $X^\wedge = \emptyset$ unless $X$ is a subset of a kernel class of $f$, in which case $X^\wedge$ is simply the corresponding kernel class of $g$.  

That is, if $d\in D$ and $X \subseteq f^{-1}\{d\}$, then

$$X^{\wedge} = \{b \in B \mid g(b) = d\} = g^{-1}\{d\}.$$

Similarly, if $Y \subseteq g^{-1}\{d\}$, then

$$Y^{\vee} = \{a \in A \mid f(a) = d\} = f^{-1}\{d\}.$$

Clearly, 

$$C = \bigcup_{d\in D} f^{-1}\{d\} \times g^{-1}\{d\}.$$

The possible epimorphisms from $A$ onto $D$ correspond to the congruences of $A$ with $|D|$ blocks.  Let $\mathrm{Con}_D(A)$ denote the set of all such congruences of $A$.  Similarly, $\mathrm{Con}_D(B)$ denotes that of $B$.

For each pair $\alpha, \beta$ with $\alpha \in \mathrm{Con}_D(A)$ and $\beta \in \mathrm{Con}_D(B)$, there correspond epimorphisms $f: A \to D$ and $g: B \to D$ such that $\alpha = \ker f$ and $\beta = \ker g$, and a fiber product

$$C_{\alpha, \beta} = \bigcup_{d\in D}f^{-1}\{d\} \times g^{-1}\{d\}.$$


Let $f: A \to D$ and $g: B \to D$ be epimorphisms and let $C = \{(a, b)\in A \times B \mid f(a) = g(b)\}$ be the corresponding fiber product. Define $h : C \to D$ by $h(a,b) = f(a)$. 

We know $\ker f = \bigcup_{d\in D}f^{-1}\{d\} \times f^{-1}\{d\}$ is a finitely generate sublattice of $F(X)^2$, where $X$ are the generators of $A$.

We know $\ker g = \bigcup_{d\in D}g^{-1}\{d\}\times g^{-1}\{d\}$ is a finitely generate sublattice of $F(Y)^2$, where $Y$ are the generators of $B$.

Let $\{(a_i, b_i) \mid 0\leq i <m\}$ be the generators of $\ker f$.

Let $\{(a'_i, b'_i) \mid 0\leq i <n\}$ be the generators of $\ker g$.
